# API Proxy

nginx proxy app for app api

## Usage

### Environment Vars

* `LISTEN_PORT` - Port to listen on (default: `8000`)
* `APP_HOST` - Hostname of app to forward requests to (default: `app`)
* `APP_PORT` - Port of the app to forward requests to (default: `9000`)
